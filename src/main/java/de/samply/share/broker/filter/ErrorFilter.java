/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.share.broker.filter;

import de.samply.share.broker.listener.StartupListener;
import de.samply.share.broker.model.EnumStartupStatus;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.omnifaces.filter.HttpFilter;
import org.omnifaces.util.Servlets;

/**
 * This web filter checks if any errors occurred during startup
 */
@WebFilter(filterName = "ErrorFilter")
public class ErrorFilter extends HttpFilter {

  private static final String STARTUP_ERROR_PAGE = "/error/startupError.xhtml?faces-redirect=true";

  @Override
  public void doFilter(HttpServletRequest request, HttpServletResponse response,
      HttpSession session, FilterChain chain) throws IOException, ServletException {

    /**
     * This is crucial, because the default encoding in tomcat is ISO-8859-1 and the HttpRequest tries to parse
     * the parameters using this default encoding.
     */
    request.setCharacterEncoding(StandardCharsets.UTF_8.displayName());

    if (StartupListener.getStartupStatus() != EnumStartupStatus.OK && !request.getRequestURI()
        .startsWith("/error/")) {
      Servlets.facesRedirect(request, response, STARTUP_ERROR_PAGE);
    } else {
      chain.doFilter(request, response);
    }
  }

}
