/*
 * This file is generated by jOOQ.
*/
package de.samply.share.broker.model.db.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Reply implements Serializable {

    private static final long serialVersionUID = -1876190715;

    private Integer id;
    private String  content;
    private Integer bankId;
    private Integer inquiryId;

    public Reply() {}

    public Reply(Reply value) {
        this.id = value.id;
        this.content = value.content;
        this.bankId = value.bankId;
        this.inquiryId = value.inquiryId;
    }

    public Reply(
        Integer id,
        String  content,
        Integer bankId,
        Integer inquiryId
    ) {
        this.id = id;
        this.content = content;
        this.bankId = bankId;
        this.inquiryId = inquiryId;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getBankId() {
        return this.bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public Integer getInquiryId() {
        return this.inquiryId;
    }

    public void setInquiryId(Integer inquiryId) {
        this.inquiryId = inquiryId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Reply (");

        sb.append(id);
        sb.append(", ").append(content);
        sb.append(", ").append(bankId);
        sb.append(", ").append(inquiryId);

        sb.append(")");
        return sb.toString();
    }
}
