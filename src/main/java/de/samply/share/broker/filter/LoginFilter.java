/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.share.broker.filter;

import de.samply.share.broker.control.LoginController;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.KeycloakSecurityContext;
import org.omnifaces.filter.HttpFilter;

/**
 * This web filter handles the code from the central authentication server. It
 * checks if there is a code query parameter and if there is, it tries to get an
 * access token using the given code.
 */
@WebFilter(filterName = "LoginFilter")
public class LoginFilter extends HttpFilter {

    private static final Logger logger = LogManager.getLogger(LoginFilter.class);

    public static final String CONFIG_FILENAME = "broker.oauth2.xml";
    public static final String OAUTH2_PROJECT_NAME = "samply";
    private static final String AUTH_ERROR_PAGE = "/error/authError.xhtml";

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, HttpSession session, FilterChain chain) throws IOException, ServletException {

        /**
         * This is crucial, because the default encoding in tomcat is ISO-8859-1 and the HttpRequest tries to parse
         * the parameters using this default encoding.
         */
        request.setCharacterEncoding(StandardCharsets.UTF_8.displayName());

        if (session == null) {
            session = request.getSession(true);
        }

        LoginController loginController = (LoginController) session.getAttribute("loginController");

        /**
         * Create a new session.
         */
        if(loginController == null) {
            loginController = new LoginController();
            session.setAttribute("loginController", loginController);
        }

        if (!loginController.getLoginValid()) {
            KeycloakSecurityContext security = (KeycloakSecurityContext) request.getAttribute(
                KeycloakSecurityContext.class.getName());
            if (security != null) {
                loginController.login(security);
            }
        }
        chain.doFilter(request, response);
    }

}
