var reallyDeleteString = "";
var deleteString = "";
var browseLabelString = "";
var removeLabelString = "";
var uploadLabelString = "";
var uploadTitleString = "";

function setEBStrings(rds, ds, bls, rls, uls, uts) {
	reallyDeleteString = rds;
	deleteString = ds;
	browseLabelString = bls;
	removeLabelString = rls;
	uploadLabelString = uls;
	uploadTitleString = uts;
}

$(document).ready(function() {	
	$(":input").keypress(function()  {
		$('.saved').hide();
	});
	if ($('#description_remaining')) {
	var max = $('#description').attr('maxlength');
	$('#description_remaining').html((max - $('#description').val().length));
		$("#description").on('input paste', function() {
			var remaining = max - $('#description').val().length;
			$('#description_remaining').html(remaining);
			if (remaining < 500 && remaining > 99) {
				$('#description_remaining').css('color', '#DBA901');
			} else if (remaining < 100) {
				$('#description_remaining').css('color', 'red');
			} else {
				$('#description_remaining').css('color', '');
			}
		});
	}
	createExposeUpload();
	createVoteUpload();
	
	createSiteSelect();
    createEventhandlers();
    // By default, check all result types
    $('.resultTypes input:checkbox').prop('checked', true);

});

function createExposeUpload() {
	var userId = $('#userId').val();
	
	$("#exposeUpload").fileinput('destroy').fileinput({
		language : 'de',
		browseLabel : browseLabelString,
		browseIcon : "<i class='fa fa-folder-open-o'></i>&nbsp;",
		showUpload : false,
		showCancel : false,
		showRemove : false,
		showPreview : false,
		showCaption : true,
		uploadUrl : '/rest/documentUpload/user/' + userId + '/expose/',
		uploadAsync : true,
		maxFileCount : 1,
		allowedFileExtensions : ['pdf'],
		maxFileSize : 10000,
		elErrorContainer: '#expose-upload-error'
	}).on("filebatchselected", function(event, files) {
	    // trigger upload method immediately after files are selected
		$("#exposeUpload").fileinput("upload");
	}).on('fileuploaded', function(event, data, previewId, index) {
		var docId = data.response.documentId;
		var docType = data.response.documentType;
		loadExposeWithId({expose_id : docId });
	});
}

function createVoteUpload() {
	if ($('#voteUpload').length) {
		var userId = $('#userId').val();
		$("#voteUpload").fileinput('destroy').fileinput({
			language : 'de',
			browseLabel : browseLabelString,
			browseIcon : "<i class='fa fa-folder-open-o'></i>&nbsp;",
			showUpload : false,
			showCancel : false,
			showRemove : false,
			showPreview : false,
			showCaption : true,
			uploadUrl : '/rest/documentUpload/user/' + userId + '/vote/',
			uploadAsync : true,
			maxFileCount : 1,
			allowedFileExtensions : ['pdf'],
			maxFileSize : 10000,
			elErrorContainer: '#vote-upload-error'
		}).on("filebatchselected", function(event, files) {
		    // trigger upload method immediately after files are selected
			$("#voteUpload").fileinput("upload");
		}).on('fileuploaded', function(event, data, previewId, index) {
			var docId = data.response.documentId;
			var docType = data.response.documentType;
			loadVoteWithId({vote_id : docId });
		});
	}
}

function createSiteSelect() {
	var $sites = $("#siteSelect"); 
    $sites.select2({
        placeholder: "",
	    allowClear: true
	}).on("select2:unselecting", function(e) {
	    $(this).data('state', 'unselected');
	}).on("select2:open", function(e) {
	    if ($(this).data('state') === 'unselected') {
	        $(this).removeData('state'); 
	        var self = $(this);
	        setTimeout(function() {
	            self.select2('close');
	        }, 1);
	    }    
	});
	
    $(".js-programmatic-multi-set-val").on("click", function() {
        $("#siteSelect > option").prop("selected","selected");// Select All Options
        $("#siteSelect").trigger("change");// Trigger change to select 2
    });
    $(".js-programmatic-multi-clear").on("click", function () { $sites.val(null).trigger("change"); });
    $(".js-programmatic-select-own").on("click", function () {
        var loggedUserSite = $('#loggedUserSite').text();
        $sites.val(getIndexOfSite($("#siteSelect option"), loggedUserSite)).trigger("change");
    });
}

function createEventhandlers() {
    $('.hasPopover').popover({
    	placement: 'right',
    	container: 'body',
    	trigger: 'focus'
    });
    
    $('.hasConfirmPopover.deleteExpose').popover({
    	placement: 'right',
    	trigger: 'focus',
    	container: 'body',
    	content: function() {
    		var elementId = $(this).parent().find('.idDiv :input').val();
    		return reallyDeleteString + "<br /><br /><button class='btn btn-success btn-block popover-submit' type='button' onclick='deleteExpose({ elementId: " + elementId + " })' >" + deleteString + "</button>";
    	},
    	html: true
    });
    
    $('.hasConfirmPopover.deleteVote').popover({
    	placement: 'right',
    	trigger: 'focus',
    	container: 'body',
    	content: function() {
    		var elementId = $(this).parent().find('.idDiv :input').val();
    		return reallyDeleteString + "<br /><br /><button class='btn btn-success btn-block popover-submit' type='button' onclick='deleteVote({ elementId: " + elementId + " })' >" + deleteString + "</button>";
    	},
    	html: true
    });
}

function handleCooperationAvailableChange(data) {
	if (data.status == "success") {
		var showVoteUpload = data.source.checked;
		createVoteUpload();
		
		if (showVoteUpload) {
			$('.voteDiv').show();
		} else {
			$('.voteDiv').hide();			
		}
	}
}

function getIndexOfSite(siteArray, siteName) {
    var result = 0;
    $.each(siteArray, function( index, entry ) {
        if (entry.label === siteName) {
            result = index + 1; // Option index is 1-based, array index here is 0-based
            return false; // to break out of the each loop
        }
    });
    return result;
}
