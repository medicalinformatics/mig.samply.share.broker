# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.3.0] - 2020-05-27
### Added
- deal with error messages in replies from clients

## [2.2.0] - 2019-08-12
### Changed
- Update to mdrfaces version with dynamic catalogues
### Fixed
- Link to exposé fixed

## [2.1.2] - 2019-08-02
### Added
- Proxy-URL parameter in keycloak config
### Fixed
- Added missing mime-mapping for .map files

## [2.1.1] - 2019-06-25
### Fixed
- Fixed bug when trying to log out from keycloak when behind a proxy

## [2.1.0] - 2019-06-19
### Added
- Show result count for inquiries
- Datasource for catalogs to support lazy loading
### Changed
- Admin page accessible via tomcat auth instead of keycloak role

## [2.0.0] - 2019-06-05
### Added
- Initial Release