/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.share.broker.control;

import com.google.common.base.Joiner;
import de.samply.share.broker.messages.Messages;
import de.samply.share.broker.model.db.tables.pojos.Consent;
import de.samply.share.broker.model.db.tables.pojos.Contact;
import de.samply.share.broker.model.db.tables.pojos.Site;
import de.samply.share.broker.model.db.tables.pojos.User;
import de.samply.share.broker.model.db.tables.pojos.UserSite;
import de.samply.share.broker.utils.db.ContactUtil;
import de.samply.share.broker.utils.db.DocumentUtil;
import de.samply.share.broker.utils.db.SiteUtil;
import de.samply.share.broker.utils.db.UserConsentUtil;
import de.samply.share.broker.utils.db.UserSiteUtil;
import de.samply.share.broker.utils.db.UserUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.keycloak.authorization.client.Configuration;
import org.keycloak.jose.jws.JWSInput;
import org.keycloak.jose.jws.JWSInputException;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.IDToken;
import org.keycloak.representations.RefreshToken;
import org.keycloak.util.JsonSerialization;
import org.omnifaces.util.Faces;

/**
 * A JSF Managed Bean that is existent for a whole session. It handles user access.
 */
@ManagedBean(name = "loginController")
@SessionScoped
public class LoginController implements Serializable {

  /**
   * The Constant SESSION_USERNAME.
   */
  private static final String SESSION_USER = "user";

  /**
   * The Constant SESSION_ROLE.
   */
  private static final String SESSION_ROLES = "roles";

  /**
   * The Constant ROLE_PM.
   */
  private static final String ROLE_PM = "project_management";

  /**
   * The Constant RESEARCHER.
   */
  private static final String ROLE_RESEARCHER = "researcher";

  /**
   * The Constant RESEARCHER.
   */
  private static final String ROLE_ADMIN = "admin";

  private static final String ADMIN_ROLEIDENTIFIER = "SEARCHBROKER_ADMIN";

  /**
   * The Constant logger.
   */
  private static final Logger logger = LogManager.getLogger(LoginController.class);

  /**
   * The user.
   */
  private User user = new User();

  /**
   * The contact.
   */
  private Contact contact = new Contact();

  /**
   * If the login is valid this value is true. False otherwise.
   */
  private Boolean loginValid = false;

  /**
   * The user-site relation
   */
  private UserSite userSite = null;
  private int newSiteId;

  private boolean userConsent = false;

  private String code = null;

  private String error = null;

  /**
   * The JWTs from OAuth2
   */
  private AccessToken accessToken;

  /**
   * The JWT Open ID token
   */
  private IDToken idToken;

  /**
   * The JWT refresh token
   */
  private RefreshToken refreshToken;

  /**
   * Gets the user.
   *
   * @return the user
   */
  public User getUser() {
    return user;
  }

  /**
   * Sets the user.
   *
   * @param user the new user
   */
  public void setUser(User user) {
    this.user = user;
  }

  /**
   * Gets the contact.
   *
   * @return the contact
   */
  public Contact getContact() {
    return contact;
  }

  /**
   * Sets the contact.
   *
   * @param contact the new contact
   */
  public void setContact(Contact contact) {
    this.contact = contact;
  }

  /**
   * @return the userSite
   */
  public UserSite getUserSite() {
    return userSite;
  }

  /**
   * @param userSite the userSite to set
   */
  public void setUserSite(UserSite userSite) {
    this.userSite = userSite;
  }

  public Boolean getLoginValid() {
    return loginValid;
  }

  /**
   * @return the newSite
   */
  public int getNewSiteId() {
    return newSiteId;
  }

  /**
   * @param newSiteId the newSiteId to set
   */
  public void setNewSiteId(int newSiteId) {
    this.newSiteId = newSiteId;
  }

  public boolean isUserConsent() {
    return userConsent;
  }

  public void setUserConsent(boolean userConsent) {
    this.userConsent = userConsent;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public Consent getLatestConsent() {
    return UserConsentUtil.getLatestConsent();
  }

  @PostConstruct
  public void init() {
  }


  /**
   * Lets the user login and sets all necessary fields
   */
  public void login(KeycloakSecurityContext security) {

    accessToken = security.getToken();
    idToken = security.getIdToken();
    try {
      refreshToken = new JWSInput(((RefreshableKeycloakSecurityContext) security).getRefreshToken())
          .readJsonContent(RefreshToken.class);
    } catch (JWSInputException e) {
      // do nothing for now
    }

    loginValid = true;

    List<String> roles = new ArrayList<>();
    String rolesConcat;

    user = UserUtil.fetchUserByAuthId(idToken.getSubject());
    if (user == null) {
      logger.debug("user not known...creating");
      user = UserUtil.createOrUpdateUser(idToken);
      FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.getString("welcome"),
          Messages.getString("newUserCreated"));
      try {
        org.omnifaces.util.Messages.addFlashGlobal(msg);
      } catch (NullPointerException e) {
        logger.debug(
            "Null pointer caught when trying to add flash message. Probably because it was called from a filter.");
      }
    }
    UserSiteUtil.createUserSiteAssociation(user);

//        for (String role : accessToken.getResourceAccess(authzClient.getConfiguration().getResource()).getRoles()) {
//            if (role.equalsIgnoreCase(CCP_OFFICE_ROLEIDENTIFIER)) {
//                logger.info("CCP Office rights granted");
//                roles.add(ROLE_CCP_OFFICE);
//            }
//            if (role.equalsIgnoreCase(ADMIN_ROLEIDENTIFIER)) {
//                logger.info("Admin rights granted");
//                roles.add(ROLE_ADMIN);
//            }
//        }

    if (roles.isEmpty()) {
      rolesConcat = ROLE_RESEARCHER;
    } else {
      rolesConcat = Joiner.on(',').join(roles);
    }

    logger.info("user: " + user.getUsername() + " ( " + user.getAuthid() + " ) signed in");
    TimeZone.setDefault(TimeZone.getTimeZone("CET"));
    setUser(user);
    setUserConsent(UserConsentUtil.hasUserGivenLatestConsent(user));
    reloadUserSite();

    contact = ContactUtil.getContactForUser(user);
    if (contact == null) {
      contact = ContactUtil.createContactForUser(user);
    }
  }

  public void logout() {
    logout(false);
  }

  /**
   * Logout and invalidate session
   */
  public void logout(boolean noRedirect) {
    accessToken = null;
    refreshToken = null;
    idToken = null;

    loginValid = false;

    // Delete old unbound exposes...
    DocumentUtil.deleteOldUnboundDocuments();

    Faces.invalidateSession();
    user = null;

    if (noRedirect) {
      return;
    }

    ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
    try {
      ((HttpServletRequest) context.getRequest()).logout();
      context.invalidateSession();
    } catch (ServletException e) {
      logger.debug("Could not logout");
    }
    try {
      context.redirect(createLogoutUrl((HttpServletRequest) context.getRequest()));
    } catch (IOException e) {
      logger.debug("Could not call the authentication server logout URL!");
    }
  }

  private String createLogoutUrl(HttpServletRequest request) {
    InputStream configStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("keycloak.json");
    try {
      Configuration config = JsonSerialization.readValue(configStream, Configuration.class);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(config.getAuthServerUrl());
      stringBuilder.append("/realms/");
      stringBuilder.append(config.getRealm());
      stringBuilder.append("/protocol/openid-connect/logout?redirect_uri=");
      stringBuilder.append(request.getScheme());
      stringBuilder.append("://");
      stringBuilder.append(request.getServerName());
      if (!isDefaultPort(request)) {
        stringBuilder.append(":");
        stringBuilder.append(request.getServerPort());
      }
      stringBuilder.append(request.getContextPath());
      return stringBuilder.toString();

    } catch (IOException e) {
      throw new RuntimeException("Could not parse configuration.", e);
    }
  }

  /**
   * Check if the port number is the default port (http->80, https->443) Port -1 should be default
   * port
   *
   * @param request the http servlet request
   * @return true if the default port is used, false otherwise
   */
  private boolean isDefaultPort(HttpServletRequest request) {
    if (request.getServerPort() < 0) {
      return true;
    } else if (request.getServerPort() == 80) {
      return "http".equalsIgnoreCase(request.getScheme());
    } else if (request.getServerPort() == 443) {
      return "https".equalsIgnoreCase(request.getScheme());
    } else {
      return false;
    }
  }

  /**
   * Get the user name of the current user from the session.
   *
   * @return the user name of the logged in user
   */
  public String getLoggedUsername() {
    // check the session
    HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance()
        .getExternalContext().getRequest();
    return ((User) request.getSession().getAttribute(SESSION_USER)).getName();
  }

  /**
   * Update contact.
   */
  public void updateContact() {
    FacesMessage facesMessage;
    String returnValue = ContactUtil.updateContact(contact);
    if (returnValue.equalsIgnoreCase("success")) {
      facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Ok",
          Messages.getString("contactUpdated"));
    } else {
      facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("error"),
          Messages.getString("contactUpdateFailed"));
    }

    updateConsent();
    if (userSite == null || !userSite.getApproved()) {
      updateSite();
    }

    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
  }

  /**
   * Update consent for a given user
   */
  private void updateConsent() {
    UserConsentUtil.updateConsent(user, userConsent);
  }

  /**
   * Update the site associated with the current user
   */
  private void updateSite() {
    if (newSiteId > 0) {
      Site newSite = SiteUtil.fetchSiteById(newSiteId);
      UserSite newUserSite = UserSiteUtil.setSiteForUser(user, newSite, false);
      setUserSite(newUserSite);
    }
  }

  /**
   * Get the list of all sites that are in the database
   *
   * @return list of all sites
   */
  public List<Site> getSites() {
    return SiteUtil.fetchSites();
  }

  /**
   * Get the (extended) name of the site associated with the current user
   *
   * @return the extended name of the site, or the short name of the site, or "-" if none is
   * available
   */
  public String getUserSiteName() {
    Site siteForUser = UserUtil.getSiteForUser(user);
    if (siteForUser != null) {
      String nameExtended = siteForUser.getNameExtended();
      if (nameExtended != null && nameExtended.length() > 0) {
        return nameExtended;
      } else {
        return siteForUser.getName();
      }
    } else {
      return "-";
    }
  }

  /**
   * Reload the site for the currently logged in user
   */
  public void reloadUserSite() {
    try {
      UserSite currentUserSite = UserSiteUtil.fetchUserSiteByUser(user);
      setUserSite(currentUserSite);
      newSiteId = getUserSite().getSiteId();
    } catch (NullPointerException npe) {
      newSiteId = -1;
    }
  }
}
