$(document).ready(function() {
	if ($('#actionsTable').length) {
        var table = $('#actionsTable').DataTable({
            "bInfo": false,
            "ordering": false,
            language: {
                url: '../resources/js/locales/datatables_de_DE.json'
            }
        });
        
        table.on('draw', function() {
            $('#actionsTable tr').each(function(i, row) {
            	var $time = $(row).find('.action-timestamp').text();
            	var currTime = new Date().getTime();
            	var compareTime = new Date($time).getTime();
            	if (currTime > compareTime) {
            		$(this).before("<tr style='background-color: #6D97B3;'><td colspan='4' style='padding: 1px 0;'/></tr>");
            		return false;
            	}
            });         	
        });
	}
	
	if ($('#newProjectsTable').length) {
        $('#newProjectsTable').DataTable({
            "bInfo": false,
            language: {
                url: '../resources/js/locales/datatables_de_DE.json'
            },
            "columnDefs": [
                { type: 'de_datetime', targets: 2 }
            ],
            "order": [[ 0, "desc" ]]
        });
        $('.unseen').parentsUntil('tbody').css('font-weight', 'bold');
	}
	
	if ($('#pendingProjectsTable').length) {
        $('#pendingProjectsTable').DataTable({
            "bInfo": false,
            language: {
                url: '../resources/js/locales/datatables_de_DE.json'
            },
            "order": [[ 0, "desc" ]]
        });
	}
	
	if ($('#activeProjectsTable').length) {
        $('#activeProjectsTable').DataTable({
            "bInfo": false,
            language: {
                url: '../resources/js/locales/datatables_de_DE.json'
            },
            "order": [[ 0, "desc" ]]
        });
	}
	
	if ($('#archivedProjectsTable').length) {
        $('#archivedProjectsTable').DataTable({
            "bInfo": false,
            language: {
                url: '../resources/js/locales/datatables_de_DE.json'
            },
            "order": [[ 0, "desc" ]]
        });
	}

    if ($('#archivedInquiriesTable').length) {
        $('#archivedInquiriesTable').DataTable({
            "bInfo": false,
            language: {
                url: 'resources/js/locales/datatables_de_DE.json'
            },
            "columnDefs": [
                { type: 'de_datetime', targets: 2 }
            ],
            "order": [[ 2, "desc" ]]
        });
    }

    if ($('#draftsTable').length) {
        $('#draftsTable').DataTable({
            "bInfo": false,
            language: {
                url: 'resources/js/locales/datatables_de_DE.json'
            },
            "columnDefs": [
                { type: 'de_datetime', targets: 2 }
            ],
            "order": [[ 2, "desc" ]]
        });
    }

    if ($('#activeInquiriesTable').length) {
        $('#activeInquiriesTable').DataTable({
            "bInfo": false,
            language: {
                url: 'resources/js/locales/datatables_de_DE.json'
            },
            "columnDefs": [
                { type: 'de_datetime', targets: 2 }
            ],
            "order": [[ 2, "desc" ]]
        });
    }

    if ($('#inquiriesWithProjectsTable').length) {
        $('#inquiriesWithProjectsTable').DataTable({
            "bInfo": false,
            language: {
                url: 'resources/js/locales/datatables_de_DE.json'
            },
            "columnDefs": [
                { type: 'de_datetime', targets: 2 }
            ],
            "order": [[ 2, "desc" ]]
        });
    }
});