/*
 * This file is generated by jOOQ.
*/
package de.samply.share.broker.model.db.enums;


import de.samply.share.broker.model.db.Public;

import javax.annotation.Generated;

import org.jooq.Catalog;
import org.jooq.EnumType;
import org.jooq.Schema;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public enum ActionType implements EnumType {

    AT_NEW_PROJECT("AT_NEW_PROJECT"),

    AT_PROJECT_ENDING_TODAY("AT_PROJECT_ENDING_TODAY"),

    AT_PROJECT_ENDING_SOON("AT_PROJECT_ENDING_SOON"),

    AT_PROJECT_ARCHIVED("AT_PROJECT_ARCHIVED"),

    AT_PROJECT_ACTIVATED("AT_PROJECT_ACTIVATED"),

    AT_PROJECT_OPENED("AT_PROJECT_OPENED"),

    AT_PROJECT_REJECTED("AT_PROJECT_REJECTED"),

    AT_PROJECT_CALLBACK_SENT("AT_PROJECT_CALLBACK_SENT"),

    AT_PROJECT_CALLBACK_RECEIVED("AT_PROJECT_CALLBACK_RECEIVED"),

    AT_USER_MESSAGE("AT_USER_MESSAGE"),

    AT_REMINDER("AT_REMINDER"),

    AT_REMINDER_EXTERNAL("AT_REMINDER_EXTERNAL"),

    AT_INQUIRY_EDITED("AT_INQUIRY_EDITED");

    private final String literal;

    private ActionType(String literal) {
        this.literal = literal;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Catalog getCatalog() {
        return getSchema() == null ? null : getSchema().getCatalog();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "action_type";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLiteral() {
        return literal;
    }
}
