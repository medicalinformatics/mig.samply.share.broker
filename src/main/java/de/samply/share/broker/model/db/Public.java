/*
 * This file is generated by jOOQ.
*/
package de.samply.share.broker.model.db;


import de.samply.share.broker.model.db.tables.Action;
import de.samply.share.broker.model.db.tables.Authtoken;
import de.samply.share.broker.model.db.tables.Bank;
import de.samply.share.broker.model.db.tables.BankSite;
import de.samply.share.broker.model.db.tables.Consent;
import de.samply.share.broker.model.db.tables.Contact;
import de.samply.share.broker.model.db.tables.Document;
import de.samply.share.broker.model.db.tables.EmailSite;
import de.samply.share.broker.model.db.tables.Inquiry;
import de.samply.share.broker.model.db.tables.InquirySite;
import de.samply.share.broker.model.db.tables.Note;
import de.samply.share.broker.model.db.tables.Project;
import de.samply.share.broker.model.db.tables.ProjectSite;
import de.samply.share.broker.model.db.tables.Reply;
import de.samply.share.broker.model.db.tables.Site;
import de.samply.share.broker.model.db.tables.Tokenrequest;
import de.samply.share.broker.model.db.tables.User;
import de.samply.share.broker.model.db.tables.UserConsent;
import de.samply.share.broker.model.db.tables.UserSite;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Catalog;
import org.jooq.Sequence;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Public extends SchemaImpl {

    private static final long serialVersionUID = 1188043546;

    /**
     * The reference instance of <code>public</code>
     */
    public static final Public PUBLIC = new Public();

    /**
     * The table <code>public.action</code>.
     */
    public final Action ACTION = de.samply.share.broker.model.db.tables.Action.ACTION;

    /**
     * The table <code>public.authtoken</code>.
     */
    public final Authtoken AUTHTOKEN = de.samply.share.broker.model.db.tables.Authtoken.AUTHTOKEN;

    /**
     * The table <code>public.bank</code>.
     */
    public final Bank BANK = de.samply.share.broker.model.db.tables.Bank.BANK;

    /**
     * The table <code>public.bank_site</code>.
     */
    public final BankSite BANK_SITE = de.samply.share.broker.model.db.tables.BankSite.BANK_SITE;

    /**
     * The table <code>public.consent</code>.
     */
    public final Consent CONSENT = de.samply.share.broker.model.db.tables.Consent.CONSENT;

    /**
     * The table <code>public.contact</code>.
     */
    public final Contact CONTACT = de.samply.share.broker.model.db.tables.Contact.CONTACT;

    /**
     * The table <code>public.document</code>.
     */
    public final Document DOCUMENT = de.samply.share.broker.model.db.tables.Document.DOCUMENT;

    /**
     * The table <code>public.email_site</code>.
     */
    public final EmailSite EMAIL_SITE = de.samply.share.broker.model.db.tables.EmailSite.EMAIL_SITE;

    /**
     * The table <code>public.inquiry</code>.
     */
    public final Inquiry INQUIRY = de.samply.share.broker.model.db.tables.Inquiry.INQUIRY;

    /**
     * The table <code>public.inquiry_site</code>.
     */
    public final InquirySite INQUIRY_SITE = de.samply.share.broker.model.db.tables.InquirySite.INQUIRY_SITE;

    /**
     * The table <code>public.note</code>.
     */
    public final Note NOTE = de.samply.share.broker.model.db.tables.Note.NOTE;

    /**
     * The table <code>public.project</code>.
     */
    public final Project PROJECT = de.samply.share.broker.model.db.tables.Project.PROJECT;

    /**
     * The table <code>public.project_site</code>.
     */
    public final ProjectSite PROJECT_SITE = de.samply.share.broker.model.db.tables.ProjectSite.PROJECT_SITE;

    /**
     * The table <code>public.reply</code>.
     */
    public final Reply REPLY = de.samply.share.broker.model.db.tables.Reply.REPLY;

    /**
     * The table <code>public.site</code>.
     */
    public final Site SITE = de.samply.share.broker.model.db.tables.Site.SITE;

    /**
     * The table <code>public.tokenrequest</code>.
     */
    public final Tokenrequest TOKENREQUEST = de.samply.share.broker.model.db.tables.Tokenrequest.TOKENREQUEST;

    /**
     * The table <code>public.user</code>.
     */
    public final User USER = de.samply.share.broker.model.db.tables.User.USER;

    /**
     * The table <code>public.user_consent</code>.
     */
    public final UserConsent USER_CONSENT = de.samply.share.broker.model.db.tables.UserConsent.USER_CONSENT;

    /**
     * The table <code>public.user_site</code>.
     */
    public final UserSite USER_SITE = de.samply.share.broker.model.db.tables.UserSite.USER_SITE;

    /**
     * No further instances allowed
     */
    private Public() {
        super("public", null);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Sequence<?>> getSequences() {
        List result = new ArrayList();
        result.addAll(getSequences0());
        return result;
    }

    private final List<Sequence<?>> getSequences0() {
        return Arrays.<Sequence<?>>asList(
            Sequences.ACTION_ID_SEQ,
            Sequences.AUTHTOKEN_ID_SEQ,
            Sequences.BANK_ID_SEQ,
            Sequences.CONSENT_ID_SEQ,
            Sequences.CONTACT_ID_SEQ,
            Sequences.DOCUMENT_ID_SEQ,
            Sequences.EMAIL_SITE_ID_SEQ,
            Sequences.INQUIRY_ID_SEQ,
            Sequences.NOTE_ID_SEQ,
            Sequences.PROJECT_ID_SEQ,
            Sequences.REPLY_ID_SEQ,
            Sequences.SITE_ID_SEQ,
            Sequences.TOKENREQUEST_ID_SEQ,
            Sequences.USER_ID_SEQ);
    }

    @Override
    public final List<Table<?>> getTables() {
        List result = new ArrayList();
        result.addAll(getTables0());
        return result;
    }

    private final List<Table<?>> getTables0() {
        return Arrays.<Table<?>>asList(
            Action.ACTION,
            Authtoken.AUTHTOKEN,
            Bank.BANK,
            BankSite.BANK_SITE,
            Consent.CONSENT,
            Contact.CONTACT,
            Document.DOCUMENT,
            EmailSite.EMAIL_SITE,
            Inquiry.INQUIRY,
            InquirySite.INQUIRY_SITE,
            Note.NOTE,
            Project.PROJECT,
            ProjectSite.PROJECT_SITE,
            Reply.REPLY,
            Site.SITE,
            Tokenrequest.TOKENREQUEST,
            User.USER,
            UserConsent.USER_CONSENT,
            UserSite.USER_SITE);
    }
}
