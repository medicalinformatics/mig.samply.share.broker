$(document).ready(function() {
	$("#documentUpload").fileinput({
		language : 'de',
		browseLabel : "Datei wählen",
		browseIcon : "<i class='fa fa-folder-open-o'></i>&nbsp;",
		removeLabel : "Entfernen",
		removeIcon : "<i class='fa fa-ban'></i>&nbsp;",
		uploadLabel : "Hochladen",
		uploadIcon : "<i class='fa fa-upload'></i>&nbsp;",
		uploadTitle : 'Dokument zur Liste hinzufügen',
		previewFileIcon : ''
	});
    
    $('#documentBoxForm').submit(function(e) {
    	$(this).find('.uploadTheFile').trigger("click");
    	e.preventDefault();
    });
    
    createEventhandlers();
    
    $('.collapsePanel').on('hide.bs.collapse', function() {
    	$(this).find('i.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-right');
    });
    $('.collapsePanel').on('show.bs.collapse', function() {
    	$(this).find('i.fa-chevron-right').removeClass('fa-chevron-right').addClass('fa-chevron-down');    	
    });
});

function createEventhandlers() {
    $('.hasPopover').popover({
    	placement: 'left',
    	container: 'body',
    	trigger: 'focus'
    });
    
    $('.hasConfirmPopover').popover({
    	placement: 'left',
    	trigger: 'focus',
    	container: 'body',
    	content: function() {
    		var elementId = $(this).parent().find('.idDiv :input').val();
    		var elementType = $(this).parent().find('.typeDiv :input').val();
    		if (elementType == "document") {
    			return "Wirklich löschen?<br /><br /><button class='btn btn-success btn-block popover-submit' type='button' onclick='deleteDocument({ elementId: " + elementId + " })' >Löschen</button>";
    		} else if (elementType == "reminder") {
    			return "Wirklich löschen?<br /><br /><button class='btn btn-success btn-block popover-submit' type='button' onclick='deleteReminder({ elementId: " + elementId + " })' >Löschen</button>";
    		}
    	},
    	html: true
    });
    
    $('.addReminder').on('click', function() {
    	$('.addReminderDiv').hide();
    	$('.newReminderDiv').show();
    });
    
    $('.newReminderDiv a').on('click', function() {
    	$('.newReminderDiv').hide();
    	$('.addReminderDiv').show();
    });

    $('#newReminderDate').datetimepicker({
        format: "D.M.YYYY",
        showTodayButton: true,
        locale: 'de',
        calendarWeeks: true,
        minDate: new Date()
    });
    
    $('#newReminderExternalDate').datetimepicker({
        format: "D.M.YYYY",
        showTodayButton: true,
        locale: 'de',
        calendarWeeks: true,
        minDate: new Date()
    });
    
    $('.date-picker, .datepicker').on('click', '.input-group-addon', function() {
    	$(this).parent().children(':input').trigger('focus');
    });
}

function handleTypeChangeEvent(data) {
	if (data.status == "success") {
		createEventhandlers();
	}
}