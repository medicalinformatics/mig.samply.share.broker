CREATE FUNCTION public.getnextappnr()
  RETURNS INTEGER AS $$
DECLARE
  current_year INTEGER;
  max_appnr    INTEGER;
BEGIN
  current_year := date_part('year', now());
  SELECT COALESCE(max(application_number), 19000000)
  INTO max_appnr
  FROM public.project;

  IF current_year * 10000 > max_appnr
  THEN
    RETURN (current_year * 10000 + 0001);
  ELSE
    RETURN max_appnr + 1;
  END IF;

END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION public.appnr_insert()
  RETURNS TRIGGER AS $appnr_insert$
BEGIN
  NEW.application_number := public.getnextappnr();
  RETURN NEW;
END;
$appnr_insert$ LANGUAGE plpgsql;

CREATE TRIGGER appnr_insert
BEFORE INSERT ON public.project
FOR EACH ROW EXECUTE PROCEDURE public.appnr_insert();