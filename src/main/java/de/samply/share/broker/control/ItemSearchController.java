/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.share.broker.control;

import com.google.common.base.Splitter;
import de.samply.common.mdrclient.domain.EnumElementType;
import de.samply.common.mdrclient.domain.Result;
import de.samply.jsf.MdrUtils;
import de.samply.share.broker.utils.Config;
import de.samply.share.broker.utils.Utils;
import de.samply.share.common.control.uiquerybuilder.AbstractItemSearchController;
import de.samply.share.common.model.uiquerybuilder.MenuItem;
import de.samply.share.common.model.uiquerybuilder.MenuItemTreeManager;
import de.samply.share.common.utils.ProjectInfo;
import de.samply.share.common.utils.SamplyShareUtils;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.omnifaces.util.Ajax;

/**
 * The MDR item search panel controller.
 */
@ManagedBean(name = "ItemSearchController")
@ViewScoped
public class ItemSearchController extends AbstractItemSearchController {

  private static final Logger logger
      = LogManager.getLogger(ItemSearchController.class);
  private static final String CFG_DEFAULT_NAMESPACES = "mdr.alwaysShow";
  /**
   * The namespace of the OSSE common data set.
   */
  private final String OSSE_CDS_NAMESPACE = "osse-cds";
  /**
   * The namespace of the OSSE ror.
   */
  private final String OSSE_ROR_NAMESPACE = "osse-ror";
  /**
   * The prefix of osse registry namespaces
   */
  private final String OSSE_NAMESPACE_PREFIX = "osse-";
  /**
   * A list of namespace names that shall always be included in the mdr browser
   */
  private List<String> defaultNamespaces;

  public ItemSearchController() {
    defaultNamespaces = new ArrayList<>();
    readDefaultNamespaces();
  }

  /**
   * Fill the list of default namespaces from the config file and include them to the search
   * namespaces
   */
  private void readDefaultNamespaces() {
    try {
      String namespaceList = Config.instance.getProperty(CFG_DEFAULT_NAMESPACES);
      defaultNamespaces = Splitter.on(";").omitEmptyStrings().trimResults()
          .splitToList(namespaceList);
      for (String namespace : defaultNamespaces) {
        addToSearchNamespaces(namespace);
      }
    } catch (Exception e) {
      logger.error("Caught Exception", e);
    }
  }

  /**
   * Gets the the private key to login at the MDR with Auth.
   *
   * @return the private key as string
   */
  @Override
  public String getPrivateKey() {
    String path =
        SamplyShareUtils.addTrailingFileSeparator(ProjectInfo.INSTANCE.getConfig().getConfigPath())
            + "key.der.txt";
    byte[] encoded;
    try {
      encoded = Files.readAllBytes(Paths.get(path));
      return new String(encoded, StandardCharsets.US_ASCII);
    } catch (IOException ex) {
      throw new RuntimeException("Private key string could not be found in " + path, ex);
    }
  }

  /**
   * @return Auth Id from config file
   */
  @Override
  public String getMdrAuthKeyId() {
    return ProjectInfo.INSTANCE.getConfig().getProperty("mdr.auth.keyId");
  }

  /**
   * Loads the items from the MDR and populates the menu items. For OSSE, load all root elements of
   * the {@link #OSSE_CDS_NAMESPACE}.
   */
  @Override
  public void resetMenuItems() {
    menuItems.clear();

    List<Result> results = new ArrayList<>();
    for (String defaultNamespace : defaultNamespaces) {
      results.addAll(getMdrRootElements(defaultNamespace).getResults());
    }

    for (Result r : results) {
      MenuItem menuItem = MenuItemTreeManager.buildMenuItem(r.getId(),
          EnumElementType.valueOf(r.getType()),
          MdrUtils.getDesignation(r.getDesignations()),
          MdrUtils.getDefinition(r.getDesignations()),
          new ArrayList<>(), null);
      menuItems.add(menuItem);
    }
  }

  @Override
  public void onDataElementGroupClick(final String mdrId) {
    MenuItem parent = MenuItemTreeManager.getMenuItem(menuItems, mdrId);

    if (MenuItemTreeManager.isItemOpen(parent)) { // just let javascript close the drawer
      MenuItemTreeManager.cleanMenuItemStyleClass(parent);
    } else {
      MenuItemTreeManager.cleanMenuItemsStyleClass(menuItems);
      MenuItemTreeManager.clearMenuItemChildren(parent);

      logger.trace("Menu " + parent.getDesignation() + ", " + parent.getMdrId() + " clicked.");

      for (Result r : getGroupMembers(mdrId)) {
        if (Utils.getAB().getMdrKeyBlacklist().contains(r.getId())) {
          logger.debug(r.getId() + " is marked as unsupported. Skipping...");
          continue;
        }
        MenuItem menuItem = MenuItemTreeManager
            .buildMenuItem(r.getId(), EnumElementType.valueOf(r.getType()),
                MdrUtils.getDesignation(r.getDesignations()),
                MdrUtils.getDefinition(r.getDesignations()), new ArrayList<>(), parent);
        MenuItemTreeManager.addMenuItem(menuItem, parent);
      }
      MenuItemTreeManager.setItemAndParentsOpen(parent);
      Ajax.update(getItemNavigationPanel().getClientId());
    }
  }
}
