/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.share.broker.utils.db;

import de.samply.share.broker.jdbc.ResourceManager;
import de.samply.share.broker.model.db.Tables;
import de.samply.share.broker.model.db.tables.pojos.BankSite;
import de.samply.share.broker.model.db.tables.pojos.Reply;
import de.samply.share.common.utils.SamplyShareUtils;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.jooq.DSLContext;

/**
 * This class provides static methods for CRUD operations for Reply Objects
 *
 * @see Reply
 */
public final class ReplyUtil {

  // Prevent instantiation
  private ReplyUtil() {
  }

  /**
   * Get the most recent replies for a given site and inquiry
   *
   * (a site can have multiple banks...so take the reply from each bank)
   *
   * @param inquiryId the id of the inquiry
   * @param siteId  the id of the site
   * @return the replies to the inquiry by the banks belonging to a site
   */
  public static List<Reply> getRepliesById(int inquiryId, int siteId) {
    List<Reply> replies = null;
    List<BankSite> bankSites = BankSiteUtil.fetchBankSitesBySiteId(siteId);
    List<Integer> bankIds = new ArrayList<>();

    if (SamplyShareUtils.isNullOrEmpty(bankSites)) {
      return replies;
    } else {
      for (BankSite bankSite : bankSites) {
        bankIds.add(bankSite.getBankId());
      }
    }

    try (Connection conn = ResourceManager.getConnection()) {
      DSLContext create = ResourceManager.getDSLContext(conn);
      replies = create.select()
          .from(Tables.REPLY)
          .where(
              Tables.REPLY.INQUIRY_ID.equal(inquiryId)
                  .and(Tables.REPLY.BANK_ID.in(bankIds))
          )
          .fetchInto(Reply.class);
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return replies;
  }
}
