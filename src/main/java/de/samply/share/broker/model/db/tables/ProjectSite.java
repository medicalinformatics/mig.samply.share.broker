/*
 * This file is generated by jOOQ.
*/
package de.samply.share.broker.model.db.tables;


import de.samply.share.broker.model.db.Indexes;
import de.samply.share.broker.model.db.Keys;
import de.samply.share.broker.model.db.Public;
import de.samply.share.broker.model.db.tables.records.ProjectSiteRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ProjectSite extends TableImpl<ProjectSiteRecord> {

    private static final long serialVersionUID = 593331252;

    /**
     * The reference instance of <code>public.project_site</code>
     */
    public static final ProjectSite PROJECT_SITE = new ProjectSite();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<ProjectSiteRecord> getRecordType() {
        return ProjectSiteRecord.class;
    }

    /**
     * The column <code>public.project_site.project_id</code>.
     */
    public final TableField<ProjectSiteRecord, Integer> PROJECT_ID = createField("project_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>public.project_site.site_id</code>.
     */
    public final TableField<ProjectSiteRecord, Integer> SITE_ID = createField("site_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * Create a <code>public.project_site</code> table reference
     */
    public ProjectSite() {
        this(DSL.name("project_site"), null);
    }

    /**
     * Create an aliased <code>public.project_site</code> table reference
     */
    public ProjectSite(String alias) {
        this(DSL.name(alias), PROJECT_SITE);
    }

    /**
     * Create an aliased <code>public.project_site</code> table reference
     */
    public ProjectSite(Name alias) {
        this(alias, PROJECT_SITE);
    }

    private ProjectSite(Name alias, Table<ProjectSiteRecord> aliased) {
        this(alias, aliased, null);
    }

    private ProjectSite(Name alias, Table<ProjectSiteRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.PROJECT_SITE_PKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<ProjectSiteRecord> getPrimaryKey() {
        return Keys.PROJECT_SITE_PKEY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<ProjectSiteRecord>> getKeys() {
        return Arrays.<UniqueKey<ProjectSiteRecord>>asList(Keys.PROJECT_SITE_PKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<ProjectSiteRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<ProjectSiteRecord, ?>>asList(Keys.PROJECT_SITE__PROJECT_SITE_PROJECT_ID_FKEY, Keys.PROJECT_SITE__PROJECT_SITE_SITE_ID_FKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjectSite as(String alias) {
        return new ProjectSite(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjectSite as(Name alias) {
        return new ProjectSite(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public ProjectSite rename(String name) {
        return new ProjectSite(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public ProjectSite rename(Name name) {
        return new ProjectSite(name, null);
    }
}
