/*
 * This file is generated by jOOQ.
*/
package de.samply.share.broker.model.db.tables;


import de.samply.share.broker.model.db.Indexes;
import de.samply.share.broker.model.db.Keys;
import de.samply.share.broker.model.db.Public;
import de.samply.share.broker.model.db.tables.records.AuthtokenRecord;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Authtoken extends TableImpl<AuthtokenRecord> {

    private static final long serialVersionUID = -985041229;

    /**
     * The reference instance of <code>public.authtoken</code>
     */
    public static final Authtoken AUTHTOKEN = new Authtoken();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<AuthtokenRecord> getRecordType() {
        return AuthtokenRecord.class;
    }

    /**
     * The column <code>public.authtoken.id</code>.
     */
    public final TableField<AuthtokenRecord, Integer> ID = createField("id", org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(org.jooq.impl.DSL.field("nextval('authtoken_id_seq'::regclass)", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * The column <code>public.authtoken.value</code>.
     */
    public final TableField<AuthtokenRecord, String> VALUE = createField("value", org.jooq.impl.SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column <code>public.authtoken.lastused</code>.
     */
    public final TableField<AuthtokenRecord, Timestamp> LASTUSED = createField("lastused", org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * Create a <code>public.authtoken</code> table reference
     */
    public Authtoken() {
        this(DSL.name("authtoken"), null);
    }

    /**
     * Create an aliased <code>public.authtoken</code> table reference
     */
    public Authtoken(String alias) {
        this(DSL.name(alias), AUTHTOKEN);
    }

    /**
     * Create an aliased <code>public.authtoken</code> table reference
     */
    public Authtoken(Name alias) {
        this(alias, AUTHTOKEN);
    }

    private Authtoken(Name alias, Table<AuthtokenRecord> aliased) {
        this(alias, aliased, null);
    }

    private Authtoken(Name alias, Table<AuthtokenRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.AUTHTOKEN_PKEY, Indexes.AUTHTOKEN_VALUE_KEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<AuthtokenRecord, Integer> getIdentity() {
        return Keys.IDENTITY_AUTHTOKEN;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<AuthtokenRecord> getPrimaryKey() {
        return Keys.AUTHTOKEN_PKEY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<AuthtokenRecord>> getKeys() {
        return Arrays.<UniqueKey<AuthtokenRecord>>asList(Keys.AUTHTOKEN_PKEY, Keys.AUTHTOKEN_VALUE_KEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Authtoken as(String alias) {
        return new Authtoken(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Authtoken as(Name alias) {
        return new Authtoken(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Authtoken rename(String name) {
        return new Authtoken(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Authtoken rename(Name name) {
        return new Authtoken(name, null);
    }
}
