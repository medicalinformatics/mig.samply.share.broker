/*
 * This file is generated by jOOQ.
*/
package de.samply.share.broker.model.db.enums;


import de.samply.share.broker.model.db.Public;

import javax.annotation.Generated;

import org.jooq.Catalog;
import org.jooq.EnumType;
import org.jooq.Schema;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public enum DocumentType implements EnumType {

    DT_EXPOSE("DT_EXPOSE"),

    DT_VOTE("DT_VOTE"),

    DT_REPORT_PROGRESS("DT_REPORT_PROGRESS"),

    DT_REPORT_FINAL("DT_REPORT_FINAL"),

    DT_EXPERT_ASSESSMENT("DT_EXPERT_ASSESSMENT"),

    DT_OTHER("DT_OTHER");

    private final String literal;

    private DocumentType(String literal) {
        this.literal = literal;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Catalog getCatalog() {
        return getSchema() == null ? null : getSchema().getCatalog();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "document_type";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLiteral() {
        return literal;
    }
}
